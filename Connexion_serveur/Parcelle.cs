﻿
namespace Decryptage
{
    public class Parcelle
    {

        private char type;
        private bool est;
        private bool sud;
        private bool ouest;
        private bool nord;

        public Parcelle(int valeur_trame)
        {
            valeur_trame = initType(valeur_trame);
            initFrontieres(valeur_trame);
        }

        private int initType(int valeur_trame)
        {
            this.type = 'x';
            if (valeur_trame - 64 > 0)
            {
                this.type = 'M';
                valeur_trame -= 64;
            }
            else if (valeur_trame - 32 > 0)
            {
                this.type = 'F';
                valeur_trame -= 32;
            }
            return valeur_trame;
        }

        private void initFrontieres(int valeur_trame)
        {
            if(valeur_trame !=0)
            {
                valeur_trame -= 8;
                this.est = true;
            }
            if(valeur_trame < 0)
            {
                valeur_trame += 8;
                this.est = false;
            }


            if (valeur_trame != 0)
            {
                valeur_trame -= 4;
                this.sud = true;
            }
            if (valeur_trame < 0)
            {
                valeur_trame += 4;
                this.sud = false;
            }


            if (valeur_trame != 0)
            {
                valeur_trame -= 2;
                this.ouest = true;
            }
            if (valeur_trame < 0)
            {
                valeur_trame += 2;
                this.ouest = false;
            }


            if (valeur_trame != 0)
            {
                valeur_trame -= 1;
                this.nord = true;
            }
            if (valeur_trame < 0)
            {
                valeur_trame += 1;
                this.nord = false;
            }

        }

        public char getType() {return this.type;}

        public bool getEst() {return this.est;}

        public bool getSud() {return this.sud;}

        public bool getOuest() {return this.ouest;}

        public bool getNord() {return this.nord;}

    }
}

