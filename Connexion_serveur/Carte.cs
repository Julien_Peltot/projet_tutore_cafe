﻿using System;

namespace Decryptage
{
    class Carte
    {
        private Parcelle[,] carte;

        public Carte(int[,] tab_trame)
        {
            this.carte = new Parcelle[10, 10];

            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    carte[x, y] = new Parcelle(tab_trame[x, y]);
                }
            }
                           
        }

        public void AfficheCarte()
        {
            for (int x=0;x<10;x++)
            {
                for(int y=0;y<10;y++)
                {
                    Console.Write(carte[x, y].getType());
                }
                Console.WriteLine();
            }
        }
    }
}
