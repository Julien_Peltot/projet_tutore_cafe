﻿using System;
using System.Text;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets


namespace Decryptage
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //public Socket(AddressFamily InterNetwork, SocketType Stream, ProtocolType Tcp);
           
                Socket the_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                the_socket.Connect("51.91.120.237", 1212); // pour nous 
                //the_socket.Connect("172.16.0.88", 1212); // pour l'iut
            try
            {
                byte[] tab = new byte[70 * sizeof(int)];
                the_socket.Receive(tab);
                String trame = Encoding.UTF8.GetString(tab);
                Console.WriteLine( "voici la trame brut : \n {0} \n ",trame);
                char[] delimiter = { '|', ':' };
                String[] trame_parcelle = trame.Split(delimiter);

                int[,] int_carte = new int[10, 10];
                Console.WriteLine("Voici la int_carte :");
                for (int x = 0; x < 10; x++)
                {
                    for (int y = 0; y < 10; y++)
                    {
                        int_carte[x, y] = int.Parse(trame_parcelle[(x*10) +y]);
                        Console.Write(int_carte[x, y] + " : ");

                    }
                    Console.WriteLine();  
                }
                Console.WriteLine();

                Carte carte = new Carte(int_carte);
                Console.WriteLine("Voici la carte :");
                carte.AfficheCarte();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
            the_socket.Shutdown(SocketShutdown.Both);
            the_socket.Close();          
        }
    }
}
